module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "flashlight"
  cidr = "172.30.0.0/22"

  azs             = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1], data.aws_availability_zones.available.names[2]]
  private_subnets = ["172.30.1.0/24", "172.30.2.0/24", "172.30.3.0/24"]

  enable_nat_gateway = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Terraform   = "true"
    Environment = "flashlight"
  }
}