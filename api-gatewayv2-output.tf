# API Gateway v2 HTTP
output "aws_apigatewayv2_api_start" {
  description = "The URL of the Flashlight Start Lambda Function"
  value       = aws_apigatewayv2_api.flashlight-start.api_endpoint
}

output "aws_apigatewayv2_api_stop" {
  description = "The URL of the Flashlight Stop Lambda Function"
  value       = aws_apigatewayv2_api.flashlight-stop.api_endpoint
}