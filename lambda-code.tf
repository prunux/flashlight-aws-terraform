
# Lambda
resource "aws_lambda_permission" "awsapigwv2_lambda_start" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.flashlight_start.function_name
  principal     = "apigateway.amazonaws.com"
  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_apigatewayv2_api.flashlight-start.id}/*/*/flashlight_start"
}

resource "aws_lambda_function" "flashlight_start" {
  filename      = "flashlight_start.zip"
  function_name = "flashlight_start"
  description   = "flashlight start"
  role          = aws_iam_role.lambda_ec2_handler.arn
  handler       = "flashlight_start.lambda_handler"
  runtime       = "python3.8"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda.zip"))}"
  source_code_hash = filebase64sha256("flashlight_start.zip")
}

resource "aws_lambda_permission" "awsapigwv2_lambda_stop" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.flashlight_stop.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_apigatewayv2_api.flashlight-stop.id}/*/*/flashlight_stop"
}

resource "aws_lambda_function" "flashlight_stop" {
  filename      = "flashlight_stop.zip"
  function_name = "flashlight_stop"
  description   = "flashlight stop"
  role          = aws_iam_role.lambda_ec2_handler.arn
  handler       = "flashlight_stop.lambda_handler"
  runtime       = "python3.8"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda.zip"))}"
  source_code_hash = filebase64sha256("flashlight_stop.zip")
}
