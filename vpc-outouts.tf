# VPC
output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "vpc_private_subnets" {
  description = "The private subnet IDs of the VPC"
  value       = module.vpc.private_subnets
}